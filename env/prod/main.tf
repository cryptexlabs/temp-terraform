module "vpc" {
  source          = "../../modules/vpc"
  project_name    = var.project_name
  env_name        = var.env_name
  cidr_block      = var.cidr_bock
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets
  region          = var.region
}

module "eks" {
  source              = "../../modules/eks"
  vpc_id              = module.vpc.vpc_id
  vpc_private_subnets = module.vpc.private_subnets
  vpc_public_subnets  = module.vpc.public_subnets
  region              = var.region
}

module "elasticache" {
  source = "../../modules/elasticache"
  cluster_id = "redis-shared"
  env_name = var.env_name
  project_name = var.project_name
  node_groups = 3
  vpc_subnets = module.vpc.private_subnets
  region = var.region
  vpc_id = module.vpc.vpc_id
  # minimum is 2
  replicas = 2
}

module "domain" {
  source = "../../modules/route53"
  region = var.region
  domain_name = "cryptexlabs.com"
}

module "cognito" {
  source = "../../modules/cognito"
  region = var.region
  project_name = var.project_name
  env_name = var.env_name
}