variable "project_name" {
  description = "Name to us for prefixes of resource names"
  default     = "cryptex"
  type        = string
}

variable "env_name" {
  description = "Environment name"
  default     = "production"
  type        = string
}

variable "cidr_bock" {
  description = "CIDR block to create resources under"
  default     = "10.0.0.0/16"
  type        = string
}

variable "private_subnets" {
  description = "Private subnets for vpc to create resources in"
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  type        = list
}

variable "public_subnets" {
  description = "Public subnets for vpc to create resources in"
  default     =  ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  type        = list
}

variable "region" {
  description = "Region where resources should be created"
  default = "us-east-1"
}