variable "vpc_id" {
  description = "VPC for cluster"
  type = string
}

variable "vpc_public_subnets" {
  description = "Public subnets for vpc"
  type = list
}

variable "vpc_private_subnets" {
  description = "Public subnets for vpc"
  type = list
}

variable "project_name" {
  description = "Name to us for prefixes of resource names"
  default     = "cryptex"
  type        = string
}

variable "env_name" {
  description = "Environment name"
  default     = "production"
  type        = string
}

variable "region" {
  description = "Region where vpc should be created"
  type = string
}