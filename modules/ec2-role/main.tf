resource "aws_iam_role" "main" {

  name = var.role_name

  assume_role_policy = <<POLICY
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Sid": "EC2RoleType",
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": "ec2.amazonaws.com"
     },
     "Effect": "Allow"
   }
 ]
}
POLICY
}