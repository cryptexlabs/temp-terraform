variable "region" {
  description = "AWS region"
  type        = string
}

variable "domain_name" {
  type = string
  description = "The domain name for the zone to be created"
}