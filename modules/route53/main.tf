resource "aws_route53_zone" "primary" {
  name = var.domain_name
}

data "aws_route53_zone" "primary" {
  zone_id = aws_route53_zone.primary.zone_id
}