module "main_vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.project_name}/vpc-${var.env_name}"

  cidr = var.cidr_block

  azs             = data.aws_availability_zones.available.names
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Name = "shared-${var.env_name}"
  }
}