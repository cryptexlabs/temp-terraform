variable "project_name" {
  description = "Name to us for prefixes of resource names"
  type = string
}

variable "env_name" {
  description = "Environment name"
  type = string
}

variable "cidr_block" {
  description = "CIDR block to create resources under"
  type = string
}

variable "private_subnets" {
  description = "Private subnets for vpc to create resources in"
  type = list
}

variable "public_subnets" {
  description = "Public subnets for vpc to create resources in"
  type = list
}

variable "region" {
  description = "Region where vpc should be created"
  type = string
}