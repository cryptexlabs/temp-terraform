variable "project_name" {
  type = string
  description = "Name of the project the pool is for"
}

variable "env_name" {
  type = string
  description = "Name of the environment"
}

variable "region" {
  type = string
  description = "The region to create the user pool and identity provider"
}