output "congito_user_pool_id" {
  value = aws_cognito_user_pool.main.id
}

output "cognito_identity_pool_id" {
  value = aws_cognito_identity_pool.main.id
}