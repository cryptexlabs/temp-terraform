resource "aws_cognito_user_pool" "main" {
  name = "${var.project_name}-${var.env_name}"
}

resource "aws_cognito_user_pool_client" "main_identity_pool" {
  name = "client_for_main_identity_pool"
  user_pool_id = aws_cognito_user_pool.main.id
}

resource "aws_cognito_identity_pool" "main" {

  identity_pool_name = "${var.project_name}${var.env_name}Main"

  cognito_identity_providers {
    client_id               = aws_cognito_user_pool_client.main_identity_pool.id
    provider_name           = "cognito-idp.${var.region}.amazonaws.com/${aws_cognito_user_pool.main.id}"
    server_side_token_check = true
  }
}