variable "region" {
  description = "Region for the cluster"
  type        = string
}

variable "cluster_id" {
  description = "ID for the cluster"
  type        = string
}

variable "public_key_path" {
  description = "Path to public key for ssh access"
  default     = "~/.ssh/cryptex.pub"
  type        = string
}

variable "node_groups" {
  description = "Number of nodes groups to create in the cluster"
  type        = string
}

variable "vpc_subnets" {
  description = "Subnets for cluster"
  type        = list
}

variable "vpc_id" {
  description = "VPC for cluster"
  type        = string
}

variable "project_name" {
  description = "Name to us for prefixes of resource names"
  type        = string
}

variable "env_name" {
  description = "Environment name"
  type        = string
}

variable "replicas" {
  type = number
}