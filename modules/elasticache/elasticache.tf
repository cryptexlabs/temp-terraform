resource "aws_security_group" "default" {
  name_prefix = "${var.project_name}-${var.env_name}"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elasticache_subnet_group" "default" {
  name       = "${var.project_name}-${var.env_name}-cache-subnet"
  subnet_ids = var.vpc_subnets
}

resource "aws_elasticache_parameter_group" "default" {
  family = "redis5.0"
  name   = var.cluster_id

  parameter {
    name  = "activerehashing"
    value = "yes"
  }

  parameter {
    name  = "min-replicas-to-write"
    value = var.replicas
  }
}

resource "aws_elasticache_replication_group" "default" {
  replication_group_id          = var.cluster_id
  replication_group_description = "Redis cluster"

  node_type            = "cache.t2.small"
  port                 = 6379
  parameter_group_name = aws_elasticache_parameter_group.default.name

  snapshot_retention_limit = 5
  snapshot_window          = "00:00-05:00"

  subnet_group_name          = aws_elasticache_subnet_group.default.name
  automatic_failover_enabled = true

  number_cache_clusters = var.replicas
}